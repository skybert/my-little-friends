#! /usr/bin/env bash

# Nice report on everything I've comitted the last (n) days.
#
# Usage: what-did-i-do [days back]
#
# If you don't pass anything to the command, it'll list work done the
# last 3 days.
#
# by torstein@escenic.com

src_dir=$HOME/src
since=
author=tkj
days_back=1

# source "${HOME}"/src/moria/src/common/text/color.sh

get_since() {
  now=$(date +%s)
  echo $(( now - (days_back * 24 * 60 * 60) ))
}

show_time_tracking() {
  local from_date=$1
  local to_date=$2

  source ${HOME}/.j.conf || exit 1
  local user_worklog=
  user_worklog=$(
    curl \
      -s \
      -u "${jira_user}:${jira_password}" \
      "${jira_url}/rest/api/2/search?jql=worklogAuthor%20%3DcurrentUser()%20and%20worklogDate%20>%3D%20${from_date}%20and%20worklogDate%20<%3D%20${to_date}")

  if [ -n "${user_worklog}" ]; then
    printf "❯❯ Logged hours on these issues in Jira:\n"
    jq --raw-output '.issues[].key' <<< "${user_worklog}" |
      sort -u |
      while read -r issue; do
        printf "— %s " "${issue}"
        jv "${issue}" | sed -rn 's#Summary .*:(.*)#\1#p'
      done

  fi
}

## $1 iso date, e.g. 2022-01-01
day_after() {
  local _date=$1

  date  -d "${_date}+1days" --iso
}

## $1 iso date, e.g. 2022-01-01
day_before() {
  local _date=$1

  date  -d "${_date}-1days" --iso
}



main() {
  if [ -n "${1}" ]; then
    date=$1
  else
    date=$(date --iso)
  fi
  if [ -n "${2}" ]; then
    author=$2
  fi

  from_date=${date}
  before_date=$(day_before "${from_date}")
  to_date=$(day_after "${from_date}")
  printf "❯ What %s has done on %s\n\n" \
         "${author}" \
         "${from_date}"
  find "${src_dir}" -maxdepth 2 -name .git -type d |
    grep -E -v 'hobbiton|my-little-friends|skybert-net' |
    while read -r d; do
      dir=$(dirname "${d}")
      (
        diff=$(
          git \
            --no-pager \
            -C "${dir}" \
            log \
            --branches \
            --since="${before_date}" \
            --until="${date}" \
            --author "${author}" \
            --pretty="format:%ci %s" \
            --no-merges \
            2>/dev/null |
            sort)
        if [ -n "${diff}" ]; then
          printf "❯❯ %s\n" "${dir//${src_dir}\//}"
          printf "%s\n\n" "${diff}"

        fi
      )
    done

  show_time_tracking "${from_date}" "${to_date}"
  show_slack_search "${from_date}" "${author}"
}

show_slack_search() {
  local _date=$1
  local _author=$2

  ## TODO don't hard code the Slack user
  _author=skybert

  cat <<EOF

❯❯ Slack search string
from:@skybert on:${_date}
EOF

}


main "$@"
