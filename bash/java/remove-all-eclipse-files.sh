#! /usr/bin/env bash

## author: torstein, tkj@stibodx.com

set -o errexit
set -o nounset
set -o pipefail

main() {
  local dir=
  dir=$(pwd)

  find "${dir}" \( -name .project -or -name .classpath \) -delete
  find "${dir}" -name .settings -type d |
    while read -r d; do
      rm -rf "${d}"
    done
}

main "$@"
