#! /usr/bin/env bash

## Find constant declarations for a given string value.
##
## author: torstein@escenic.com
set -o errexit
set -o nounset
set -o pipefail
shopt -s nullglob

find_constants_declared_for() {
  local dir=$1
  local query=$2

  grep --recursive --line-number --fixed-strings "\"${query}\"" "${dir}" |
    grep src/main/java |
    grep -w static |
    grep -w String
}


main() {
  local dir query

  if [ $# -eq 2 ]; then
    dir=$1
    shift 1
  elif [ $# -eq 1 ]; then
    dir=$(pwd)
  fi

  query=$1
  find_constants_declared_for "${dir}" "${1}"
}

main "$@"
