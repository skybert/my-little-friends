#! /usr/bin/env bash

## author: torstein, tkj@stibodx.com

set -o errexit
set -o nounset
set -o pipefail

main() {
  rm -rf ~/.config/JetBrains/
  rm -rf ~/.cache/JetBrains/
  rm -rf ~/.local/share/JetBrains/
  test -d .idea && rm -rf .idea
}

main "$@"
