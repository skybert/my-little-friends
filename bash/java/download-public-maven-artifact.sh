#! /usr/bin/env bash

## author: torstein, tkj@stibodx.com

set -o errexit
set -o nounset
set -o pipefail

_move_settings_xml_out_of_the_way() {
  test -r "${HOME}/.m2/settings.xml" &&
    mv -v  "${HOME}/.m2/settings.xml" "${HOME}/.m2/settings.xml.orig"
}

_restore_settings_xml() {
  test -r "${HOME}/.m2/settings.xml.orig" &&
    mv -v  "${HOME}/.m2/settings.xml.orig" "${HOME}/.m2/settings.xml"
}

main() {
  if [ $# -lt 1 ]; then
    printf "Usage: ${BASH_SOURCE[0]} %s\\n" "org.foo:foo-core:1.2.3"
    exit 1
  fi

  local _artifact_coordinates=$1

  _move_settings_xml_out_of_the_way
  mvn dependency:get -Dartifact="${_artifact_coordinates}"
  _restore_settings_xml
}

main "$@"
