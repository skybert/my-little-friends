;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; 97 RSS
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(setq elfeed-feeds
      '(
        ("https://archlinux.org/feeds/news/" unix)
        ("https://boehs.org/in/blog.xml" security)
        ("https://feeds.feedburner.com/chrisheilmann" web)
        ("https://grumpygamer.com/rss" fun)
        ("https://irreal.org/blog/?feed=rss2" emacs)
        ("https://sachachua.com/blog/category/emacs/feed/" emacs)
        ("https://scummvm.org/feeds/atom" fun)
        ("https://seclists.org/rss/oss-sec.rss" security)
        ("https://skybert.net/feeds/atom-feed.xml" unix)
        ("https://vin01.github.io/piptagole/feed.xml" security)
        ("https://www.osnews.com/files/recent.xml" unix)
        ("https://www.schneier.com/feed/atom/" security)
        ))
