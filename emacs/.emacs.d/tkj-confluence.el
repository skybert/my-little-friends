;; Simple mode for editing Confluence Wiki markup.
;;
;; author: torstein at skybert.net
(defvar my-highlights nil "My syntax highlight rules")

(setq my-highlights
      '(("h\\([1-9]\\). \\|" . font-lock-function-name-face)
        ("h\\([1-9]\\). \\(.*\\)" . (2 font-lock-constant-face))
        ("{code}" . font-lock-preprocessor-face)
        ("{quote}" . font-lock-doc-face)
        ))

(define-derived-mode skybert/confluence-mode
  text-mode
  "sky/conf"
  "Major mode for editing Confluence wiki markup."
  (setq font-lock-defaults '(my-highlights)))
