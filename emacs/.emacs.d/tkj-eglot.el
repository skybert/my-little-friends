;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; 79 eglot (language server) & dape (debugger)
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(use-package eglot
  :config
  (setq eglot-report-progress nil)

  (add-to-list
   'eglot-server-programs
   `((java-mode java-ts-mode) .
     ("jdtls"
      :initializationOptions
      (:bundles ["/home/torstein/.m2/repository/com/microsoft/java/com.microsoft.java.debug.plugin/0.51.1/com.microsoft.java.debug.plugin-0.51.1.jar"]))))

  :hook
  ((python-mode . eglot-ensure))

  :bind
  (("<f7>" . dape-step-in)
   ("<f8>" . dape-next)
   ("<f9>" . dape-continue)))

(use-package dape
  :after eglot
  :config
  (add-to-list
   'dape-configs
   `(java8705
     modes (java-mode java-ts-mode)
     hostname "localhost"
     port (lambda () (eglot-execute-command (eglot-current-server) "vscode.java.startDebugSession" nil))
     :request "attach"
     :hostname "localhost"
     :port 8705
     :type "java")))

(use-package eglot-java :after eglot)
(use-package dap-mode :after lsp-mode :config (dap-auto-configure-mode))
