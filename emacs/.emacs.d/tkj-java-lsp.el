(use-package lsp-mode
  :ensure t
  :hook
  ((lsp-mode . lsp-enable-which-key-integration))
  :config
  (setq lsp-enable-file-watchers nil
        lsp-headerline-breadcrumb-enable nil
        )

  ;; Performance tweaks, see
  ;; https://github.com/emacs-lsp/lsp-mode#performance
  (setq gc-cons-threshold 100000000)
  (setq read-process-output-max (* 1024 1024)) ;; 1mb
  (setq lsp-idle-delay 0.100)

  :bind
  (:map lsp-mode-map
        (("\C-\M-g" . lsp-find-implementation)
         ("M-RET" . lsp-execute-code-action)))
  )

(use-package hydra :ensure t)
(use-package lsp-ui :ensure t)
(use-package which-key :ensure t :config (which-key-mode))
(use-package lsp-java
  :ensure t
  :config
  (add-hook 'java-mode-hook 'lsp)
  (setq lsp-java-vmargs
        '(
          "-noverify"
          "-Xmx4G"
          "-XX:+UseG1GC"
          "-XX:+UseStringDeduplication"
          "-javaagent:/home/torstein/.m2/repository/org/projectlombok/lombok/1.18.32/lombok-1.18.32.jar"
          )

        lsp-java-java-path "/usr/lib/jvm/java-21-openjdk/bin/java"

        ;; Don't organise imports on save
        lsp-java-save-action-organize-imports nil

        ;; Don't format my source code (I use Maven for enforcing my
        ;; coding style)
        lsp-java-format-enabled nil))
(use-package dap-mode :after lsp-mode :config (dap-auto-configure-mode))
(use-package lsp-treemacs)
