;;; three-amigos.el --- An emacs major mode for editing Three Amigos

;; Copyright (C) 2023  Torstein Krause Johansen

;; Author: Torstein Krause Johansen
;; Keywords: languages
;; Version: 0.0.1

;; Licensed under the Apache License, Version 2.0 (the "License");
;; you may not use this file except in compliance with the License.
;; You may obtain a copy of the License at
;;
;;     http://www.apache.org/licenses/LICENSE-2.0
;;
;; Unless required by applicable law or agreed to in writing, software
;; distributed under the License is distributed on an "AS IS" BASIS,
;; WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
;; See the License for the specific language governing permissions and
;; limitations under the License.

;;; Commentary:

;; A mode for editing The Three Amigos, aka Gherkin.
;;
;; For more about gherkin, see https://cucumber.io/docs/bdd/who-does-what/

;;; Code:


(defconst three-amigos-re
  (regexp-opt
   '(
     "given" "Given" "GIVEN"
     "when" "When" "WHEN"
     "then" "Then" "THEN"
     "and" "And" "AND"
     "background" "Background" "BACKGROUND"
     "scenario" "Scenario" "SCENARIO"
     "feature" "Feature" "FEATURE"
     )
   'words))

(defconst three-amgios-keywords
  `(("<.*>" . font-lock-constant-face)
    ("#.*$" . font-lock-comment-face)
    ,three-amigos-re
    ("@[^:]*" . font-lock-variable-name-face)
    ("\"[^\"]*\"" . font-lock-string-face)
    ("'[^']*'" . font-lock-string-face)))

(define-derived-mode three-amigo-mode text-mode
  "The Three Amigos"
  :group 'three-amigos-mode
  ;; Comments
  (make-local-variable 'comment-start)
  (setq comment-start "# ")
  ;; Font-lock support
  (setq font-lock-defaults '(three-amgios-keywords))
  ;; Key maps
  ;;(define-key three-amigo-mode-map (kbd "C-c C-x") 'whatever)
  )

(provide 'three-amigo-mode)
;;; three-amigos.el ends here
