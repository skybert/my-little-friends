tmux new-window -n "🐃emacs" 'TERM=xterm-24bits emacsclient -nw'
tmux new-window -n "🔨files" zsh
tmux new-window -n "📦lxd" zsh
tmux new-window -n "🔒um" zsh
tmux new-window -n "🤵build" zsh
tmux new-window -n "🧙 ece-scripts" zsh
