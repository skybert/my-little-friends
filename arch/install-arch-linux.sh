#! /usr/bin/env bash

## Usage:
##
## To install a regular machine, do:
##    $ ./install-arch-linux.sh
## To install a work machine, do:
##    $ IS_WORK_MACHINE=true ./install-arch-linux.sh
##
## author: torstein aka skybert

set -o errexit
set -o nounset
set -o pipefail

packages=(
  base
  base-devel
  bat               # A better cat
  bc                # Math on the command line
  bridge-utils
  btop
  calibre           # Ebooks
  colordiff
  cups              # Printing
  curl              # HTTP client for the command line
  deluge-gtk
  dict-jargon
  direnv
  dmidecode
  dnsmasq
  dosfstools
  efibootmgr
  emacs             # Hotel California
  etckeeper
  fakeroot
  fastfetch
  feh
  firefox
  fortune-mod
  freetype2
  fwupd
  fzf
  gimp
  git
  gnu-netcat
  grub
  gwenview
  hunspell-en_gb
  i3-wm
  i3lock
  i3status
  inetutils
  iwd
  jq
  kernel-modules-hook
  kitty
  ksnip
  less
  libreoffice-fresh
  libsepol
  linux
  linux-firmware
  lsof              # Which app has file open?
  make
  man-db
  mpv
  neofetch
  net-tools
  nmap              # Network, security
  noto-fonts
  noto-fonts-cjk
  noto-fonts-emoji
  openssh
  pandoc-cli
  pandoc-crossref
  patch
  pavucontrol       # Sound
  pelican
  perl-json-xs      # json → yaml
  perl-yaml-libyaml # json → yaml
  picom             # Transparency, rounded corners++
  pipewire          # Sound
  pipewire-alsa     # Sound
  pipewire-pulse    # Sound
  pkgconf
  podman
  pwvucontrol
  python38
  rofi-greenclip
  rofi-pass
  rsync
  rust
  scrot             # Screenshots
  signal-desktop
  sof-firmware      # Firmware to get sound working, Intel Meteor Lake-P HD Audio
  strace
  sudo
  tmux
  ttf-nerd-fonts-symbols-mono
  ttf-symbola
  tty-clock
  ufw               # Firewall, security
  unzip
  vim
  virt-manager
  wget
  wireplumber       # Sound
  x11-ssh-askpass   # SSH
  xorg-xclock
  xorg-xdm          # Lean and mean login manager
  xorg-xeyes
  xorg-xkill
  xorg-xload
  xorg-xrandr
  xscreensaver
  xsel
  words            # To make "look <word>" work
  zip
  zsh              # My favourite shell
  zsh-autosuggestions # zsh killer app
)

packages_home=(
  calibre
)

packages_work=(
  d2
  bash-language-server 
  eclipse-java
  google-chrome
  gopls
  intellij-idea-community-edition
  jdk21-openjdk
  jdtls
  kafka
  languagetool      # Spelling and grammar checker
  mariadb
  maven
  npm               # Node package manager
  obs-studio        # Video production
  osv-scanner       # Vulnerability scanner
  playwright
  pyright           # Python
  python-pipenv     # Python
  qemu-full
  shellcheck
  siege             # Load testing
  silicon           # Convert code to terminal graphics
  visualvm          # JVM inspector
  websocat          # Websocket CLI debugger
  wireshark-qt      # Network debugger
  xmlstarlet        # XML & xpath
  zaproxy
)

aur_packages=(
  dict-jargon
  dmg2img                     # Virtual machines
  google-chrome
  noto-color-emoji-fontconfig
  obs-backgroundremoval       # Video production
  rofi-greenclip              # Clipboard manager
  spotify
  ttf-symbola
  tty-clock
)

aur_packages_home=(
)

aur_packages_work=(
  amazon-ssm-agent-bin
  aws-cli-v2
  aws-session-manager-plugin
  devspace-bin      # Openshift
  eclipse-java      # Java
  fernflower-git    # Java decompiler by JetBrains, makers of IntelliJ
  jdtls             # Java
  mdatp-bin         # Anti virus
  nvm               # Run many versions of Node
  obs-backgroundremoval # Video production
  openconnect
)

install_locales() {
  echo en_GB.UTF-8 UTF-8  >> /etc/locale.gen
  locale-gen
  echo LANG="en_GB.UTF-8" >> /etc/locale.conf
}

install_aur() {
  git -C /usr/local/src clone https://github.com/Morganamilo/paru.git
  chown -R torstein /usr/local/src/paru
  (cd /usr/local/src/paru && su - torstein -c ' makepkg -si')
}

install_packages() {
  pacman -Syu "${packages[*]}"
  if "${IS_WORK_MACHINE:-false}"; then
    pacman -Syu "${packages_work[*]}"
  else
    pacman -Syu "${packages_home[*]}"
  fi
}

install_aur_packages() {
  paru -Syu "${aur_packages[*]}"
  if "${IS_WORK_MACHINE:-false}"; then
    paru -Syu "${aur_packages_work[*]}"
  else
    paru -Syu "${aur_packages_home[*]}"
  fi
}

main() {
  declare -F |
    grep -v main |
    sed -nr 's#.*(install_.*)#\1#p' |
    while read -r fn; do
      printf "%s ...\\n" "${fn}"
      $fn
    done
}

main "$@"
