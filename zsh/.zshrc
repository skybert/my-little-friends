############################################## -*- sh -*-
# Torstein's ZSH conf
##############################################

## Debub ZSH start up time, 1/2
# zmodload zsh/zprof

## ZSH function path
fpath=( "$HOME/.zsh/zsh-functions" $fpath )

## ZSH completion of various commands
# test -r ~/.zsh/zsh-completions/zsh-completions.plugin.zsh &&
#   source ~/.zsh/zsh-completions/zsh-completions.plugin.zsh
# autoload -U compinit && compinit

## Colours
test -r /usr/lib/kitty/terminfo/x/xterm-kitty && export TERM=xterm-kitty

## Bash-like navigation
autoload -U select-word-style
select-word-style bash

## Aliases
test -r $HOME/.bashrc.aliases && source $HOME/.bashrc.aliases

## Editor
# See https://emacsredux.com/blog/2020/07/16/running-emacs-with-systemd/
# export ALTERNATE_EDITOR=''
export EDITOR=vim
# zsh will set vim bindings if EDITOR is set to vim, so we'll need to
# explicitly tell it to use Emacs bindings:
bindkey -e

## PATH
test -d /usr/lib/jvm/java-21-openjdk &&
  export JAVA_HOME=/usr/lib/jvm/java-21-openjdk
export PATH=${HOME}/bin:\
${HOME}/.local/bin:\
${JAVA_HOME}/bin:\
${HOME}/src/skybert/my-little-friends/bash:\
${HOME}/src/skybert/tc/bin:\
${HOME}/src/skybert/hobbiton/dev-commands:\
${HOME}/src/build-tools/release/bin:\
${HOME}/src/cannon/bin:\
${HOME}/src/dr/bin:\
/opt/apache-maven-3.6.1/bin:\
/snap/bin:\
${PATH}

## Maven
export MAVEN_OPTS="
  -Xms4096m
  -Xmx4096m
  -Djava.awt.headless=true
"

# Enable fuzzy search
test -r /usr/share/doc/fzf/examples/key-bindings.zsh &&
  source /usr/share/doc/fzf/examples/key-bindings.zsh
test -r /usr/share/fzf/key-bindings.zsh &&
  source /usr/share/fzf/key-bindings.zsh
test -r /usr/local/share/zsh/site-functions/_fzf_completion && {
  autoload _fzf_key_bindings _fzf_completion
  _fzf_key_bindings
  _fzf_completion
}

# History
setopt INC_APPEND_HISTORY
setopt SHARE_HISTORY
setopt HIST_EXPIRE_DUPS_FIRST
setopt HIST_IGNORE_DUPS
HISTSIZE=999999999
SAVEHIST=$HISTSIZE
HISTFILE=$HOME/.zsh_history

# Locale
locale -a | grep -q -w -i en_GB.utf8 && export LC_ALL=en_GB.UTF-8

# Flatpak
export XDG_DATA_DIRS=/var/lib/flatpak/exports/share:\
$HOME/.local/share/flatpak/exports/share:\
/usr/local/share:\
/usr/share:\
$XDG_DATA_DIRS

# Man pages
export MANPAGER=less

## Exit shell without having to disown the background processes. This
## makes the ZSH behavour like that of BASH.
setopt NO_HUP
setopt NO_CHECK_JOBS

## Don't fail if using regexp in commands like "find -name *.zip" and
## the regexp doesn't match (in the current directory). Behaviour thus
## becomes the same as in bash.
unsetopt nomatch

## Auto suggestions
test -r /usr/share/zsh-autosuggestions/zsh-autosuggestions.zsh &&
  source /usr/share/zsh-autosuggestions/zsh-autosuggestions.zsh
test -r /usr/share/zsh/plugins/zsh-autosuggestions/zsh-autosuggestions.zsh &&
  source /usr/share/zsh/plugins/zsh-autosuggestions/zsh-autosuggestions.zsh
test -r /usr/local/share/zsh-autosuggestions/zsh-autosuggestions.zsh &&
  source /usr/local/share/zsh-autosuggestions/zsh-autosuggestions.zsh
test -r ~/.zsh/zsh-autosuggestions/zsh-autosuggestions.zsh &&
  source ~/.zsh/zsh-autosuggestions/zsh-autosuggestions.zsh

## In-shell syntax highlighting
test -r /usr/share/zsh-syntax-highlighting/zsh-syntax-highlighting.zsh &&
  source /usr/share/zsh-syntax-highlighting/zsh-syntax-highlighting.zsh

## My very own prompt
export PROMPT='%(?.%F{green}$.%F{red}$)%f '

## Show Git branch to the right
autoload -Uz vcs_info
precmd_vcs_info() { vcs_info }
precmd_title() { print -Pn "\e]0;%n@%m: %~\a" }
precmd_functions+=(  precmd_title precmd_vcs_info )
setopt prompt_subst
RPROMPT=\$vcs_info_msg_0_
zstyle ':vcs_info:git:*' formats '%F{240}%b%f' # '%b'
# Remove git/right prompt on previous lines
setopt transient_rprompt

## Set window title to the current directory
precmd () {
  print -Pn "\e]0;%n@%m: %~\a"
}

## Let virsh see the same machines as virt-manager
export LIBVIRT_DEFAULT_URI=qemu:///system

## Support machine local overrides and private additions
if [ -r "${HOME}"/.zshrc.private ]; then
  source $HOME/.zshrc.private
fi

node_package_manager() {
  test -r /usr/share/nvm/nvm.sh &&
    source /usr/share/nvm/nvm.sh
  test -r /usr/share/nvm/install-nvm-exec &&
    source /usr/share/nvm/install-nvm-exec
}

# Mouse cursor theme
export XCURSOR_THEME=FlatbedCursors-White
export XCURSOR_SIZE=32

# SSH agent
if ! ssh-add -l &> /dev/null; then
  test -r ~/.ssh/agent && source ~/.ssh/agent &> /dev/null
fi

## Debub ZSH start up time, 2/2
# zprof
