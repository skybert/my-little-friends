# -*- sh -*-
#
# Torstein's extra zsh settings when doing a presentation.

# My regular setup uses a dark themed optimised colour output
cat='bat  --theme GitHub --style plain --pager=never'
less='bat --theme GitHub --style plain'
unset MANPAGER
