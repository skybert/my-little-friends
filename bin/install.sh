#! /bin/bash

src=$HOME/src/skybert/my-little-friends

_install_vim() {
  mkdir -p ~/.vim
  ln -sf "${src}/vim/.vimrc" ~/
}

_install_top() {
  ln -sf "${src}/top/.toprc" ~/
}

_install_i3() {
  mkdir -p ~/.config/i3
  test -r ~/.config/i3/config && rm ~/.config/i3/config
  ln -sf "${src}/i3/config" ~/.config/i3/

  mkdir -p ~/bin
  ln -sf "${src}/bash/tkj" ~/bin/
}

## Example usage:
##
##  install_dir picom ~/.config
##
##  will remocreate ~/.config/picom -> /path/to/picom
##
## $1 :: the app, e.g. picom
## $2 :: the target parent directory, e.g. ~/.config
install_dir() {
  local app=$1
  local target_parent_dir=$2

  local src_dir=${src}/${app}
  local target_dir=${target_parent_dir}/${app}

  test -d "${target_parent_dir}" || mkdir -p "${target_parent_dir}"
  test -d "${target_dir}" && rm -rf "${target_dir}"
  test -r "${src_dir}" && ln -sf "${src_dir}" "${target_parent_dir}/."
}

_install_picom() {
  install_dir picom ~/.config
}

_install_rofi() {
  test -d ~/.config/rofi && rm -rf ~/.config/rofi
  mkdir -p ~/.config/rofi/themes
  # config for old rofi
  ln -sf "${src}/rofi/config" ~/.config/rofi/
  # config for new rofi
  ln -sf "${src}/rofi/config.rasi" ~/.config/rofi/
  ln -sf "${src}/rofi/themes/sweet.rasi" ~/.config/rofi/themes/
}

_install_greenclip() {
  test -r ~/.config/greenclip.toml && rm ~/.config/greenclip.toml
  mkdir -p ~/.config
  ln -sf "${src}/greenclip/greenclip.toml" ~/.config/
}

_install_x() {
  # XDG
  test -r ~/.config/user-dirs.dirs && rm ~/.config/user-dirs.dirs
  test -r ~/.config/user-dirs.locale && rm ~/.config/user-dirs.locale
  ln -sf "${src}/xdg/.config/user-dirs.dirs" ~/.config/
  ln -sf "${src}/xdg/.config/user-dirs.locale" ~/.config/

  # 24 bit colours
  tic -x -o ~/.terminfo "${src}/x/xterm-24bit.terminfo"

  # Mapping the keyboard
  ln -sf "${src}/x/.xmodmaprc" ~/
}

_install_git() {
  ln -sf "${src}/git/.gitignore" ~/
  ln -sf "${src}/git/.gitconfig-work" ~/.gitconfig
  ln -sf "${src}/git/.gitconfig-private" "${src}/.gitconfig"
}

_install_kitty() {
  mkdir -p ~/.config/kitty
  ln -sf "${src}/kitty/kitty.conf" ~/.config/kitty/
  ln -sf "${src}/kitty/kitty.presentation.conf" ~/.config/kitty/
  ln -sf "${src}/kitty/themes" ~/.config/kitty/

  mkdir -p ~/bin
  ln -sf /usr/bin/firefox ~/bin/
}

_install_bash() {
  ln -sf "${src}/bash/.bashrc" ~/
  ln -sf "${src}/bash/.bashrc.aliases" ~/
  ln -sf "${src}/bash/.inputrc" ~/
}

_install_zsh() {
  test -r ~/.zshrc && rm ~/.zshrc
  ln -sf "${src}/zsh/.zshrc" ~/
  mkdir -p ~/.zsh
}

_install_emacs() {
  test -d ~/.emacs.d && rm -rf ~/.emacs.d
  ln -sf "${src}/emacs/.emacs" ~/.emacs

  # dir for 3rd party packages
  mkdir -p ~/.emacs.d/elpa

  # emacs wrapper
  test -d ~/bin || mkdir ~/bin
  ln -sf "${src}/bash/emacs" ~/bin/

  # org-pretty-table is not in MELPA yet
  test -d /usr/local/src/org-pretty-table || 
    git \
      -C /usr/local/src \
      clone --quiet \
      https://github.com/Fuco1/org-pretty-table.git
}

_install_tmux() {
  test -r ~/.tmux.conf && rm ~/.tmux.conf
  ln -sf "${src}"/tmux/.tmux.conf ~/
  mkdir -p ~/.tmux
  ln -sf "${src}"/tmux/skybert-theme.tmux ~/.tmux
}

_install_screen() {
  test -r ~/.sreenrc && rm ~/.screenrc
  ln -sf "${src}"/screen/.screenrc ~/
}

_not_install_gtk2() {
  ln -sf "${src}/gtk-2.0/.gtkrc-2.0" ~/
}

## Download theme and icons from https://www.gnome-look.org/p/1253385/
##
## The corresponding Firefox theme is called "sweet-dark":
## https://addons.mozilla.org/es/firefox/addon/sweet-dark/
_not_install_gtk3() {
  mkdir -p ~/.config
  test -r ~/.config/gtk-3.0 && rm -rf ~/.config/gtk-3.0
  ln -sf "${src}/gtk-3.0" ~/.config/

  # https://github.com/EliverLara/Sweet-folders/archive/refs/heads/master.zip
  mkdir -p ~/.icons
  test -r ~/tmp/Sweet-Purple.tar.tar &&
    tar -C ~/.icons -xf ~/tmp/Sweet-Purple.tar.tar

  mkdir -p ~/.themes
  test -r ~/tmp/Sweet-Dark-v40.tar.xz &&
    tar -C ~/.themes -xf ~/tmp/Sweet-Dark-v40.tar.xz
}

not__install_gtk_theme() {
  cd /tmp || exit 1
  wget --continue --quiet https://github.com/EliverLara/Sweet/archive/refs/heads/master.zip
  rm -rf /tmp/Sweet-master ~/.themes/Sweet
  mkdir -p ~/.themes
  unzip -q /tmp/master.zip -d /tmp
  mv /tmp/Sweet-master ~/.themes/Sweet
  gsettings set org.gnome.desktop.interface gtk-theme "Sweet"
  gsettings set org.gnome.desktop.wm.preferences theme "Sweet"
}

main() {
  declare -f | sed -nr 's#^(_install_.*)\(\)#\1#p' |
    sort |
    while read f; do
      printf "⤷ %s\\n" "${f//_install_}"
      $f
  done
}

main "$@"
