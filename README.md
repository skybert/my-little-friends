The wee scripts and configuration files I cannot live without.


## These are the commands I use to set up a new computer

```
$ mkdir src && cd src
$ git clone git@gitlab.com:skybert/my-little-friends.git
$ cd my-little-friends
$ ./bin/install.sh
```

## Why is it called my-little-friends?

Why is it called `my-little-friends` and not `my-small-friends` or
`my-friends`? The reason is, `my-little-friends` is taken from a line
in the Beatles song "With A Little Help From My Friends":

> Oh, I get by with a little help from my friends

Since `I get by with a little help from my friends` was a tad too long
for a repo name, I picked some of the "best" words and it became
`my-little-friends` 😉
