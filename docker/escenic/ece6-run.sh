#! /usr/bin/env bash

## author: torstein@escenic.com
set -o errexit
set -o nounset
set -o pipefail
shopt -s nullglob

main() {
  # compile
  # tcc

  # copy
  cp /home/torstein/src/content-engine/./engine/engine-webservice/target/engine-webservice-develop-SNAPSHOT/WEB-INF/lib/engine-webservice-develop-SNAPSHOT.jar \
     "$(pwd)"

  # create containers
  docker-compose \
    --file ece6-docker-compose.yaml \
    up \
    --build

  # create publication
  drshell \
    editor \
    ece \
    create-publication \
    --publication ding
}

main "$@"
