from skybert/escenic-base:latest

maintainer torstein

run echo " \n\
profiles: \n\
  search: \n\
    install: yes \n\
    indexer_ws_uri: http://editor:8080/indexer-webservice/index/ \n\
\n\
packages: \n\
  - name: escenic-content-engine-7.0 \n\
" >> /etc/ece-install.yaml


run bash -x ece-install -f /etc/ece-install.yaml

# Monkey patching
copy ece-scripts/usr/share/escenic/ece-scripts/ece.d/start.sh \
     /usr/share/escenic/ece-scripts/ece.d/start.sh
copy ece-scripts/usr/bin/ece /usr/bin/ece

copy ece7-search.endpoint /usr/bin/
cmd ["/usr/bin/ece7-search.endpoint"]
