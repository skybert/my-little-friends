## Torstein's dockerised Emacs, yes with GUI
##
## Build:
##    $ su -c 'echo 0 > /proc/sys/kernel/randomize_va_space'
##    $ docker build -t skybert/emacs -f emacs.dockerfile .
##    $ su -c 'echo 2 > /proc/sys/kernel/randomize_va_space'
## Run:
##    ./emacs.docker

from debian:buster

## Dependencies for getting and building Emacs
run export DEBIAN_FRONTEND=noninteractive && \
    apt-get update && \
    apt-get install -y \
            autoconf \
            automake \
            build-essential \
            gnutls-dev \
            imagemagick \
            libdbus-1-dev \
            libgif-dev \
            libgtk2.0-dev \
            libjansson-dev \
            libjpeg-dev \
            libjson-xs-perl \
            libmagick++-dev \
            libncurses-dev \
            libpng-dev \
            libtiff-dev \
            libx11-dev \
            libxpm-dev \
            texinfo \
            wget

run cd /tmp && \
    wget --quiet https://alpha.gnu.org/gnu/emacs/pretest/emacs-27.0.90.tar.xz && \
    tar xzf emacs-27.0.90.tar.gz

run cd /tmp/emacs-27.0.90 && \
    ./autogen.sh && \
    ./configure && \
    make && \
    make install

run adduser --quiet --gecos "Torstein Krause Johansen,,,,"  torstein

## Other dependencies. Add contrib and non-free to the sources list as
## the official image doesn't enable this by default.
run export DEBIAN_FRONTEND=noninteractive && \
    echo 'deb http://ftp.no.debian.org/debian buster contrib non-free' >> /etc/apt/sources.list && \
    apt-get update && \
    apt-get install -y \
            aspell \
            aspell-en \
            aspell-no \
            autoconf \
            automake \
            build-essential \
            curl \
            exuberant-ctags \
            firefox-esr \
            fonts-symbola \
            fortunes \
            git \
            gnutls-dev \
            imagemagick \
            jq \
            libdbus-1-dev \
            libgif-dev \
            libgtk2.0-dev \
            libjpeg-dev \
            libjson-xs-perl \
            libmagick++-dev \
            libncurses-dev \
            libpng-dev \
            libtiff-dev \
            libx11-dev \
            libxpm-dev \
            maildir-utils \
            maven \
            mu4e \
            openjdk-11-jdk-headless \
            offlineimap \
            procps \
            shellcheck \
            silversearcher-ag \
            subversion \
            texinfo \
            texlive-fonts-recommended \
            texlive-latex-base \
            texlive-latex-recommended \
            texlive-xetex \
            unzip \
            xfonts-terminus \
            xmlstarlet \
            wget

# Setting the system timezone (you can override this when running the
# container with docker run -e TZ='<timezone>')
run ln -fs /usr/share/zoneinfo/Europe/Oslo /etc/localtime
run dpkg-reconfigure -f noninteractive tzdata

# run emacs as user
workdir /home/torstein
user torstein
cmd emacs
