## Build:
##    docker build -t skybert/tsc -f tsc.dockerfile .
## Run:
##    ./tsc.docker


from node

run npm install -g typescript@1.7.5

cmd tsc
