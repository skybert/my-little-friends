
return {
    color_scheme = "Dracula (Official)",
    -- color_scheme = 'Sweet Eliverlara (Gogh)',
    tab_bar_at_bottom = true,
    use_fancy_tab_bar = false,
    window_decorations = "RESIZE",
    audible_bell = "Disabled"
}

